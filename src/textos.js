export const TEXTOS = {
    stitulo: ["Joc","Juego","Game"],
    sganado: ["Has guanyat!", "Has ganado!", "Congratulations!"],
    srecargar: ["Fes un altre", "Haz otro", "Continue"],
    spartidasganadas: ["Partides guanyades","Partidas ganadas","Won games"],
    sratxa: ["Ratxa","Racha","Streak"],
    svuelveIntentar: ["Ho sento, torna-ho a intentar", "Lo siento, vuelve a intentarlo", "Sorry, try it again"]
}
