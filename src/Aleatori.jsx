import React from "react";
import { API_URL, IMG_URL, audios } from "./Utils";
import { Link } from "react-router-dom";
//importem dos variables/constants amb la url de la api, les definim dins Utils una sola vegada per tota la aplicació
//importem component Link de navegació que fem servir en aquest component Lista
import { TEXTOS } from "./textos";



class Aleatori extends React.Component {

  //mètode constructor PRIMER en executar-se
  constructor(props) {
    super(props);
    //definim state només amb variable pictograma com a array buida
    this.state = {
      pictograma: [],
      contador: 0,
      ratxa: 0,
      partidasGanadas: 0,
      respostaBona: 0,
      tipoActual: "tipo_CA"
    };

    this.calculaAleatoris = this.calculaAleatoris.bind(this);
    this.clica = this.clica.bind(this);

  }

  //mètode TERCER en executar-se
  componentDidMount() {
    //connectem a la API demanant els "pictograma"
    //la url completa on fem fetch és: "http://192.168.1.10:3000/api/pictograma"
    //el mètode per defecte és GET, no caldria indicar-lo, ho afegim per mostrar com es fa
    fetch(API_URL + "tea_tea?_p=0&_size=100", { method: "GET" })
      .then(data => data.json())
      .then(dadesConvertides => {
        //console.log(dadesConvertides);
        this.setState({ pictograma: [...this.state.pictograma, ...dadesConvertides] }, this.calculaAleatoris);
      })
      .catch(err => console.log(err));

    // , teas: teas, id: teas[0].id 
    fetch(API_URL + "tea_tea?_p=1&_size=100", { method: "GET" })
      .then(data => data.json())
      .then(dadesConvertides => {
        //console.log(dadesConvertides);
        this.setState({ pictograma: [...this.state.pictograma, ...dadesConvertides] }, this.calculaAleatoris);
      })
      .catch(err => console.log(err));  
  }


  calculaAleatoris() {

    let numPictograma = this.state.pictograma.length;
    let aleatori1 = Math.floor(Math.random() * numPictograma) + 1;
    let aleatori2 = aleatori1;
    let aleatori3 = aleatori1;

    while (aleatori1 === aleatori2) {
      aleatori2 = Math.floor(Math.random() * numPictograma) + 1;
    }

    while (aleatori1 === aleatori3 || aleatori2 === aleatori3) {
      aleatori3 = Math.floor(Math.random() * numPictograma) + 1;
    }

    let respostaBona;
    let de1a3 = Math.floor(Math.random() * 3) + 1;
    switch (de1a3) {
      case 1:
        respostaBona = aleatori1;
        break;
      case 2:
        respostaBona = aleatori2;
        break;
      case 3:
        respostaBona = aleatori3;
        break;
      default:
        break;

    }

    this.setState({ aleatori1, aleatori2, aleatori3, respostaBona, ganador: "res" }, 
    function(){
      this.refs.audio1.pause();
      this.refs.audio1.load();
      this.refs.audio2.pause();
      this.refs.audio2.load();
      this.refs.audio3.pause();
      this.refs.audio3.load();
      this.refs.audio4.pause();
      this.refs.audio4.load();
      this.refs.audio5.pause();
      this.refs.audio5.load();
      this.refs.audio6.pause();
      this.refs.audio6.load();
      this.refs.audio7.pause();
      this.refs.audio7.load();
      this.refs.audio8.pause();
      this.refs.audio8.load();
      this.refs.audio9.pause();
      this.refs.audio9.load();
    })

  }


  clica(idTriat) {
    let nouContador = this.state.contador + 1;
    if (idTriat === this.state.pictograma[this.state.respostaBona].id) {
      //ha guanyat
      this.setState({ ganador: "si", contador: nouContador, partidasGanadas: this.state.partidasGanadas + 1 })
    } else {
      //ha perdut
      this.setState({ ganador: "no", contador: nouContador })
    }

    let novaRatxa = this.state.ratxa + 1;
    if (idTriat === this.state.pictograma[this.state.respostaBona].id) {
      //ha guanyat
      this.setState({ ganador: "si", ratxa: novaRatxa });
    } else {
      //ha perdut
      this.setState({ ratxa: 0 })
    }
  }

  //mètode render, SEGON en executar-se
  //es torna a executar cada vegada que es canvia el State amb this.setState()
  render() {
    //si no tenim encara cap producte a this.state.pictograma render retorna missatge "cargando datos"
    if (this.state.pictograma.length === 0 || this.state.respostaBona === 0) {
      return <h4>Zero cosas</h4>;
    }

    let missatge = <></>;

    if (this.state.ganador === "no") {
      missatge = (
        <>
          <h1>{TEXTOS.svuelveIntentar[this.props.idioma]}</h1>
          <h3 onClick={() => this.clica(this.state.pictograma[this.state.aleatori2].id)}>
            {this.state.pictograma[this.state.aleatori2].nombre}</h3>
        </>
      );
    }

    if (this.state.ganador === "si") {
      missatge = (
        <>
          <h1> {TEXTOS.sganado[this.props.idioma]} </h1>
          <button className="btn btn-danger" onClick={this.calculaAleatoris} >{TEXTOS.srecargar[this.props.idioma]}  </button>
        </>
      );
    }

    console.log(this.state.respostaBona);
    console.log(this.state.pictograma[this.state.respostaBona]);

    let tipoIdioma = "tipo_CA";
    if (this.props.idioma === 1) tipoIdioma = "tipo_ES";
    if (this.props.idioma === 2) tipoIdioma = "tipo_EN";

    return (
      <>
        <h1>{TEXTOS.stitulo[this.props.idioma]}</h1>

        <img src={IMG_URL + this.state.pictograma[this.state.respostaBona].pictograma} />
        <br />
        <button className="btn btn-primary" onClick={() => this.clica(this.state.pictograma[this.state.aleatori1].id)}>
          {this.state.pictograma[this.state.aleatori1][tipoIdioma]}</button>
        <br />

        <button style={{ display: this.props.idioma === 0 ? "inline-block" : "none" }}>
          <audio ref="audio1" controls><source src={"http://192.168.1.10:8080/tea/audios/ca/" + this.state.pictograma[this.state.aleatori1].audio_CA} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 1 ? "inline-block" : "none" }}>
          <audio ref="audio2" controls><source src={"http://192.168.1.10:8080/tea/audios/es/" + this.state.pictograma[this.state.aleatori1].audio_ES} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 2 ? "inline-block" : "none" }}>
          <audio ref="audio3" controls><source src={"http://192.168.1.10:8080/tea/audios/en/" + this.state.pictograma[this.state.aleatori1].audio_EN} /></audio>
        </button>
        <br />

        <button className="btn btn-primary" onClick={() => this.clica(this.state.pictograma[this.state.aleatori2].id)}>
          {this.state.pictograma[this.state.aleatori2][tipoIdioma]}
        </button>
        <br />

        <button style={{ display: this.props.idioma === 0 ? "inline-block" : "none" }}>
          <audio ref="audio4" controls><source src={"http://192.168.1.10:8080/tea/audios/ca/" + this.state.pictograma[this.state.aleatori2].audio_CA} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 1 ? "inline-block" : "none" }}>
          <audio ref="audio5" controls><source src={"http://192.168.1.10:8080/tea/audios/es/" + this.state.pictograma[this.state.aleatori2].audio_ES} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 2 ? "inline-block" : "none" }}>
          <audio ref="audio6" controls><source src={"http://192.168.1.10:8080/tea/audios/en/" + this.state.pictograma[this.state.aleatori2].audio_EN} /></audio>
        </button>
        <br />

        <button className="btn btn-primary" onClick={() => this.clica(this.state.pictograma[this.state.aleatori3].id)}>
          {this.state.pictograma[this.state.aleatori3][tipoIdioma]}</button>
        
        <br />

        <button style={{ display: this.props.idioma === 0 ? "inline-block" : "none" }}>
          <audio ref="audio7" controls><source src={"http://192.168.1.10:8080/tea/audios/ca/" + this.state.pictograma[this.state.aleatori3].audio_CA} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 1 ? "inline-block" : "none" }}>
          <audio ref="audio8" controls><source src={"http://192.168.1.10:8080/tea/audios/es/" + this.state.pictograma[this.state.aleatori3].audio_ES} /></audio>
        </button>
        <button style={{ display: this.props.idioma === 2 ? "inline-block" : "none" }}>
          <audio ref="audio9" controls><source src={"http://192.168.1.10:8080/tea/audios/en/" + this.state.pictograma[this.state.aleatori3].audio_EN} /></audio>
        </button>
        <br />

        {missatge}

        <h5>{TEXTOS.spartidasganadas[this.props.idioma]}: {this.state.partidasGanadas}/{this.state.contador}</h5>
        <h5>{TEXTOS.sratxa[this.props.idioma]}: {this.state.ratxa}</h5>
      </>
    );
  }
}
export default Aleatori;