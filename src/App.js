import React from "react";
import Aleatori from "./Aleatori"
import "bootstrap/dist/css/bootstrap.min.css"

class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      idioma: 0
    }
  }

  render() {

    return (
      <>
        <Aleatori idioma={this.state.idioma} />
        <button onClick={() => this.setState({ idioma: 0 })} >CA</button>
        <button onClick={() => this.setState({ idioma: 1 })} >ES</button>
        <button onClick={() => this.setState({ idioma: 2 })} >EN</button>
      </>

    );
  }

}

export default App;